<html>
<head>
    <title>StudentRegister</title>
</head>
<?php
function __autoload($class_name) {
    require_once $class_name . '.class.php';
}
require_once('auth_pdo.php');

$studReg = new StudentRegister($db);

if(isset($_GET['id']) && ctype_digit($_GET['id']))
{
    $id = intval($_GET['id']);
    if($student = $studReg->visStudent($id)) {

        print("<br/>");
        print("<a>");
        print("Navn:". htmlentities($student->hentNavn()) . "<br/>\n");
        print("Klasse: ". htmlentities($student->hentKlasseNavn()) . "<br />\n");
        print("Mobil: " . htmlentities($student->hentMobil()) . "<br />\n");
        print("Epost: ". htmlentities($student->hentEpost()) . "<br />\n");
        print("</a>");
        print("<br/>");
    }

    else {
        echo "Beklager, fant ingen poster!";
    }
}
else{
    print("<h1>Studenter:</h1><br />");
    $studenter = $studReg->visAlle();

    foreach ($studenter as $student )
    {
        print("<a href=" . $_SERVER['PHP_SELF'] . "?id=" . $student->hentId() . ">". $student->hentNavn() . "</a><br/>\n");
    }
}


?>

<body>

<div class="container">
    <h1>Opprett student</h1>

    <form action="StudentRegister.class.php" method="POST" class="main-form">

        <div class="form-group">
            <label>etternavn</label>
            <input type="text" name="etternavn" id="etternavn" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $etternavn ?>">
        </div>

        <div class="form-group">
            <label>fornavn</label>
            <input type="text" name="email" id="email" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $fornavn ?>">
        </div>

        <div class="form-group">
            <label>klasse</label>
            <input type="text" name="email" id="email" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $klasse ?>">
        </div>

        <div class="form-group">
            <label>mobil</label>
            <input type="text" name="email" id="email" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $mobil ?>">
        </div>

        <div class="form-group">
            <label>www</label>
            <input type="text" name="email" id="email" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $www ?>">
        </div>

        <div class="form-group">
            <label>epost</label>
            <input type="text" name="email" id="email" class="gt-input"
                   value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $epost ?>">
        </div>

        <input type="submit" class="gt-button" value="Submit">


    </form>
<!--
    <form action="StudentRegister.class.php" method="post">
        <p>
            <label for="etternavn">etternavn:</label>
            <input type="text" name="etternavn" id="etternavn">
        </p>
        <p>
            <label for="fornavn">fornavn:</label>
            <input type="text" name="fornavn" id="fornavn">
        </p>
        <p>
            <label for="emailAddress">Email Address:</label>
            <input type="text" name="email" id="emailAddress">
        </p>
        <p>
            <label for="klasse">klasse:</label>
            <input type="text" name="klasse" id="klasse">
        </p>
        <p>
            <label for="mobil">mobil:</label>
            <input type="text" name="email" id="email">
        </p>
        <p>
            <label for="www">www:</label>
            <input type="text" name="www" id="www">
        </p>
        <p>
            <label for="epost">Email Address:</label>
            <input type="text" name="epost" id="epost">
        </p>
        <input type="submit" value="Submit">
    </form>-->


</div>
</body>
</html>
